import rsgislib
from rsgislib import imagecalc
import os

#Form monthly mean composite images, from a set of inputs organised into monthly directories

indir = '/Path/to/monthly/data/2018/01'

inputImages = [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith('LEE.tif')]

print (inputImages)


outputImage = '../Output/ForExample/S1_ESDS_mean_jan_2018_lee.kea'


rsgislib.imagecalc.calcMultiImgBandStats(inputImages, outputImage, rsgislib.SUMTYPE_MEAN, "KEA",rsgislib.TYPE_32FLOAT)


indir = '/Path/to/monthly/data/2018/02'

inputImages = [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith('LEE.tif')]

print (inputImages)


outputImage = '../Output/ForExample/S1_ESDS_mean_feb_2018_lee.kea'


rsgislib.imagecalc.calcMultiImgBandStats(inputImages, outputImage, rsgislib.SUMTYPE_MEAN, "KEA",rsgislib.TYPE_32FLOAT)


indir = '/Path/to/monthly/data/2018/03'

inputImages = [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith('LEE.tif')]

print (inputImages)


outputImage = '../Output/ForExample/S1_ESDS_mean_march_2018_lee.kea'


rsgislib.imagecalc.calcMultiImgBandStats(inputImages, outputImage, rsgislib.SUMTYPE_MEAN, "KEA",rsgislib.TYPE_32FLOAT)


indir = '/Path/to/monthly/data/2018/04'

inputImages = [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith('LEE.tif')]

print (inputImages)


outputImage = '../Output/ForExample/S1_ESDS_mean_april_2018_lee.kea'


rsgislib.imagecalc.calcMultiImgBandStats(inputImages, outputImage, rsgislib.SUMTYPE_MEAN, "KEA",rsgislib.TYPE_32FLOAT)


indir = '/Path/to/monthly/data/2018/05'

inputImages = [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith('LEE.tif')]

print (inputImages)


outputImage = '../Output/ForExample/S1_ESDS_mean_may_2018_lee.kea'


rsgislib.imagecalc.calcMultiImgBandStats(inputImages, outputImage, rsgislib.SUMTYPE_MEAN, "KEA",rsgislib.TYPE_32FLOAT)


indir = '/Path/to/monthly/data/2018/06'

inputImages = [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith('LEE.tif')]

print (inputImages)


outputImage = '../Output/ForExample/S1_ESDS_mean_june_2018_lee.kea'


rsgislib.imagecalc.calcMultiImgBandStats(inputImages, outputImage, rsgislib.SUMTYPE_MEAN, "KEA",rsgislib.TYPE_32FLOAT)



indir = '/Path/to/monthly/data/2018/07'

inputImages = [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith('LEE.tif')]

print (inputImages)


outputImage = '../Output/ForExample/S1_ESDS_mean_july_2018_lee.kea'


rsgislib.imagecalc.calcMultiImgBandStats(inputImages, outputImage, rsgislib.SUMTYPE_MEAN, "KEA",rsgislib.TYPE_32FLOAT)


indir = '/Path/to/monthly/data/2018/08'

inputImages = [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith('LEE.tif')]

print (inputImages)


outputImage = '../Output/ForExample/S1_ESDS_mean_aug_2018_lee.kea'


rsgislib.imagecalc.calcMultiImgBandStats(inputImages, outputImage, rsgislib.SUMTYPE_MEAN, "KEA",rsgislib.TYPE_32FLOAT)



indir = '/Path/to/monthly/data/2018/09'

inputImages = [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith('LEE.tif')]

print (inputImages)


outputImage = '../Output/ForExample/S1_ESDS_mean_sept_2018_lee.kea'


rsgislib.imagecalc.calcMultiImgBandStats(inputImages, outputImage, rsgislib.SUMTYPE_MEAN, "KEA",rsgislib.TYPE_32FLOAT)


indir = '/Path/to/monthly/data/2018/10'

inputImages = [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith('LEE.tif')]

print (inputImages)


outputImage = '../Output/ForExample/S1_ESDS_mean_oct_2018_lee.kea'


rsgislib.imagecalc.calcMultiImgBandStats(inputImages, outputImage, rsgislib.SUMTYPE_MEAN, "KEA",rsgislib.TYPE_32FLOAT)


indir = '/Path/to/monthly/data/2018/11'

inputImages = [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith('LEE.tif')]

print (inputImages)


outputImage = '../Output/ForExample/S1_ESDS_mean_nov_2018_lee.kea'


rsgislib.imagecalc.calcMultiImgBandStats(inputImages, outputImage, rsgislib.SUMTYPE_MEAN, "KEA",rsgislib.TYPE_32FLOAT)


indir = '/Path/to/monthly/data/2018/12'

inputImages = [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith('LEE.tif')]

print (inputImages)


outputImage = '../Output/ForExample/S1_ESDS_mean_dec_2018_2018_lee.kea'


rsgislib.imagecalc.calcMultiImgBandStats(inputImages, outputImage, rsgislib.SUMTYPE_MEAN, "KEA",rsgislib.TYPE_32FLOAT)

indir = '/Path/to/monthly/data/2019/01'

inputImages = [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith('LEE.tif')]

print (inputImages)


outputImage = '../Output/ForExample/S1_ESDS_mean_jan_2019_lee.kea'


rsgislib.imagecalc.calcMultiImgBandStats(inputImages, outputImage, rsgislib.SUMTYPE_MEAN, "KEA",rsgislib.TYPE_32FLOAT)


indir = '/Path/to/monthly/data/2019/02'

inputImages = [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith('LEE.tif')]

print (inputImages)


outputImage = '../Output/ForExample/S1_ESDS_mean_feb_2019_lee.kea'


rsgislib.imagecalc.calcMultiImgBandStats(inputImages, outputImage, rsgislib.SUMTYPE_MEAN, "KEA",rsgislib.TYPE_32FLOAT)


indir = '/Path/to/monthly/data/2019/03'

inputImages = [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith('LEE.tif')]

print (inputImages)


outputImage = '../Output/ForExample/S1_ESDS_mean_march_2019_lee.kea'


rsgislib.imagecalc.calcMultiImgBandStats(inputImages, outputImage, rsgislib.SUMTYPE_MEAN, "KEA",rsgislib.TYPE_32FLOAT)


indir = '/Path/to/monthly/data/2019/04'

inputImages = [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith('LEE.tif')]

print (inputImages)


outputImage = '../Output/ForExample/S1_ESDS_mean_april_2019_lee.kea'


rsgislib.imagecalc.calcMultiImgBandStats(inputImages, outputImage, rsgislib.SUMTYPE_MEAN, "KEA",rsgislib.TYPE_32FLOAT)


indir = '/Path/to/monthly/data/2019/05'

inputImages = [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith('LEE.tif')]

print (inputImages)


outputImage = '../Output/ForExample/S1_ESDS_mean_may_2019_lee.kea'


rsgislib.imagecalc.calcMultiImgBandStats(inputImages, outputImage, rsgislib.SUMTYPE_MEAN, "KEA",rsgislib.TYPE_32FLOAT)


indir = '/Path/to/monthly/data/2019/06'

inputImages = [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith('LEE.tif')]

print (inputImages)


outputImage = '../Output/ForExample/S1_ESDS_mean_june_2019_lee.kea'


rsgislib.imagecalc.calcMultiImgBandStats(inputImages, outputImage, rsgislib.SUMTYPE_MEAN, "KEA",rsgislib.TYPE_32FLOAT)



indir = '/Path/to/monthly/data/2019/07'

inputImages = [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith('LEE.tif')]

print (inputImages)


outputImage = '../Output/ForExample/S1_ESDS_mean_july_2019_lee.kea'


rsgislib.imagecalc.calcMultiImgBandStats(inputImages, outputImage, rsgislib.SUMTYPE_MEAN, "KEA",rsgislib.TYPE_32FLOAT)


indir = '/Path/to/monthly/data/2019/08'

inputImages = [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith('LEE.tif')]

print (inputImages)


outputImage = '../Output/ForExample/S1_ESDS_mean_aug_2019_lee.kea'


rsgislib.imagecalc.calcMultiImgBandStats(inputImages, outputImage, rsgislib.SUMTYPE_MEAN, "KEA",rsgislib.TYPE_32FLOAT)



indir = '/Path/to/monthly/data/2019/09t'

inputImages = [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith('LEE.tif')]

indir = '/Path/to/monthly/data/2019/09'

inputImages = [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith('LEE.tif')]

print (inputImages)


outputImage = '../Output/ForExample/S1_ESDS_mean_sept_2019_lee.kea'


rsgislib.imagecalc.calcMultiImgBandStats(inputImages, outputImage, rsgislib.SUMTYPE_MEAN, "KEA",rsgislib.TYPE_32FLOAT)


indir = '/Path/to/monthly/data/2019/10'

inputImages = [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith('LEE.tif')]

print (inputImages)


outputImage = '../Output/ForExample/S1_ESDS_mean_oct_2019_lee.kea'


rsgislib.imagecalc.calcMultiImgBandStats(inputImages, outputImage, rsgislib.SUMTYPE_MEAN, "KEA",rsgislib.TYPE_32FLOAT)


indir = '/Path/to/monthly/data/2019/11'

inputImages = [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith('LEE.tif')]

print (inputImages)


outputImage = '../Output/ForExample/S1_ESDS_mean_nov_2019_lee.kea'


rsgislib.imagecalc.calcMultiImgBandStats(inputImages, outputImage, rsgislib.SUMTYPE_MEAN, "KEA",rsgislib.TYPE_32FLOAT)


indir = '/Path/to/monthly/data/2019/12'

inputImages = [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith('LEE.tif')]

print (inputImages)


outputImage = '../Output/ForExample/S1_ESDS_mean_dec_2019_2019_lee.kea'


rsgislib.imagecalc.calcMultiImgBandStats(inputImages, outputImage, rsgislib.SUMTYPE_MEAN, "KEA",rsgislib.TYPE_32FLOAT)