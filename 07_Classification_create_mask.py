import rsgislib
from rsgislib import imageutils

#Generate a valid pixel mask which will be an input in classification script

inImg = 'path/to/input/image/.kea'
outImg = 'path/to/output/image_VldMsk.kea'

gdalformat = 'KEA'

rsgislib.imageutils.genValidMask(inImg, outImg, gdalformat, nodata=-9999)