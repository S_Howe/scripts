import rsgislib
import rsgislib.vectorutils
import rsgislib.imageutils
import rsgislib.classification
import rsgislib.classification.classsklearn
from sklearn.ensemble import ExtraTreesClassifier


#rasterize larch vector file (training data)
input_img = '/Path/to/Input/timeseries_modl_coeffs.kea'

input_vec_file = '/Path/to/Input/Larch_Stands.gpkg'

vec_lyr = 'Larch_Stands'

out_img_larch = '/Path/to/Input/Larch_Training_raster.kea'

rsgislib.vectorutils.rasteriseVecLyr(input_vec_file, vec_lyr, input_img, out_img_larch, gdalformat='KEA')

#rasterize non-larch vector file (training data)
input_vec_file = '/Path/to/Input/Non_Larch_Stands.gpkg'
vec_lyr = 'Non_Larch_Stands'
out_img_non_larch = '/Path/to/Input/Non_Larch_Training_raster.kea'
rsgislib.vectorutils.rasteriseVecLyr(input_vec_file, vec_lyr, input_img, out_img_non_larch, gdalformat='KEA')

imgs_info = []
name = 'Coeffs_20182019'
bands=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]

imgs_info.append(rsgislib.imageutils.ImageBandInfo(input_img, name, bands))

#extract pixel values for the classification
#larch
larch_sample_h5 = '/Path/to/Input/larch_samples.h5'
rsgislib.imageutils.extractZoneImageBandValues2HDF(imgs_info, out_img_larch, larch_sample_h5, 1)
#non larch
non_larch_sample_h5 = '/Path/to/Input/non_larch_samples.h5'
rsgislib.imageutils.extractZoneImageBandValues2HDF(imgs_info, out_img_non_larch, non_larch_sample_h5, 1)

#split data into training and testing datasets
#larch
larch_sample_train_h5 = '/Path/to/larch_samples_train.h5'
larch_sample_test_h5 = '/Path/to/larch_samples_test.h5'
rsgislib.imageutils.splitSampleHDF5File(larch_sample_h5, larch_sample_train_h5, larch_sample_test_h5, 1000, 42)
#non larch
non_larch_sample_train_h5 = '/Path/to/non_larch_samples_train.h5'
non_larch_sample_test_h5 = '/Path/to/non_larch_samples_test.h5'
rsgislib.imageutils.splitSampleHDF5File(non_larch_sample_h5, non_larch_sample_train_h5, non_larch_sample_test_h5, 1000, 42)


from sklearn.model_selection import GridSearchCV

grid_search = GridSearchCV(ExtraTreesClassifier(bootstrap=True), param_grid={'n_estimators':[10,20,50,100,250,500,1000], 'max_depth':[2,4,6,8]})
skclf = classsklearn.train_sklearn_classifer_gridsearch(cls_train_info, 500,grid_search)


#Train the classifier
#larch
cls_train_info = dict()
cls_train_info['Larch'] = rsgislib.classification.ClassSimpleInfoObj(id=1, fileH5='/Path/to/larch_samples_train.h5', red=45, green=104, blue=199)
#non larch
cls_train_info['Non-Larch'] = rsgislib.classification.ClassSimpleInfoObj(id=2, fileH5='/Path/to/non_larch_samples_train.h5', red=171, green=1, blue=101)
rsgislib.classification.classsklearn.train_sklearn_classifier(cls_train_info, skclf)



# Get numerical feature importances
importances = list(skclf.feature_importances_)
# List of tuples with variable and importance
feature_importances = [(feature, round(importance, 11)) for feature, importance in zip(bands, importances)]
# Sort the feature importances by most important first
feature_importances = sorted(feature_importances, key = lambda x: x[1], reverse = True)
# Print out the feature and importances 
[print('Variable: {:20} Importance: {}'.format(*pair)) for pair in feature_importances];

#Apply the classifier
out_cls_img = '/Path/to/Output/20182019_coeffs_Classification.kea'
img_msk = '/Path/to/Input/timeseries_modl_VldMsk.kea'
rsgislib.classification.classsklearn.apply_sklearn_classifer(cls_train_info, skclf, img_msk, 1, imgs_info, out_cls_img, 'KEA', classClrNames=True)