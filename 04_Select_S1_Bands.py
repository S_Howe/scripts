import rsgislib
from rsgislib import imageutils
import os

#Select data for a paticualr polarisation, in this case VH

indir='Input Directory Path'

imageList= [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith('lee.kea')]

for image in imageList:
	bands = [2]
	outputVH = os.path.splitext(image)[0]+'_VH.kea'
	rsgislib.imageutils.selectImageBands(image, outputVH, 'KEA', rsgislib.TYPE_32FLOAT, bands)
