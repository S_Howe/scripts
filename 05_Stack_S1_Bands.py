import os
import rsgislib
from rsgislib import imageutils

#Stack the monthly composites for a paticualr band to form a 24band image where each band is one month of the 2 year period. This can be used to extract a temporal signature pixels or areas.
  
imageList= ['List of paths to input images' ]

bandNamesList = ['jan18', 'feb18', 'mar18', 'abr18', 'may18', 'jun18', 'jul18', 'aug18', 'sep18', 'oct18', 'nov18', 'dec18', 'jan19',
 'feb19', 'mar19', 'abr19', 'may19', 'jun19', 'jul19', 'aug19', 'sep19', 'oct19', 'nov19', 'dec19']
 
outputImage='For_Example_VH_2018_2019_MonthlyMean_Stack.kea'
gdalformat = 'KEA'
gdaltype = rsgislib.TYPE_32FLOAT

imageutils.stackImageBands(imageList, bandNamesList, outputImage, None, -9999, gdalformat, gdaltype)
