import rsgislib
from rsgislib import imagefilter
import os

#Apply a Lee filter to all images in directory

indir='Input Directory Path'

imageList= [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith('.tif')]

for inputImage in imageList:
	outImgFile = os.path.splitext(inputImage)[0]+'_LEE.tif'
	imagefilter.applyLeeFilter(inputImage, outImgFile, 3, 3, "GTiff", rsgislib.TYPE_32FLOAT)
    