import rsgislib
from rsgislib import imageutils

#Prior to forming monthly composites, it is sometimes neccesary to warp inputs to a common extent

imagelist=['List of paths to input images to be warped']

inRefImg= 'Path of reference image'

for inImg in imagelist:
    outImg=inImg.split('.tif') [0]+'_warp.tif'
    gdalformat='GTiff'
    interpMethod='nearestneighbour'
    rsgislib.imageutils.resampleImage2Match(inRefImg, inImg, outImg, gdalformat, interpMethod)
