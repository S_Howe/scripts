from rsgislib.timeseries import modelfitting

#Fit a time series model and produce an 11 band image coinatining coefficients.
#See RSGISLib for further details

sen1_imgs_lut = './Input/timeseries_list.json'
coeffs_img = './Output/For/Example/timeseries_modl_VH_OLS_coeffs.kea'
model_type = 'LASSO'
gdalformat = 'KEA'
modelfitting.get_ST_model_coeffs(sen1_imgs_lut, coeffs_img, gdalformat = 'KEA', bands=[1], num_processes=4, model_type = 'LASSO')
